# README #

Code and examples from my lecture at Högskolan Dalarn 2014-05-22
DI/IoC MVC/MVVM

### What is this repository for? ###

* Contains presentation and code examples

### Bookrecommendations ###

Head First - Design Patterns
http://www.adlibris.com/se/bok/head-first-design-patterns-9780596007126

Contact
klas.broberg(That at sign)sogeti.se
[twitter.com/crapitan](http://twitter.com/crapitan)